import * as React from 'react'
import { NextPage } from 'next'

import { PageWrapper, Content, Column } from 'components/layout'
import { Stack, UnstyledButton, Box } from 'components/design-system'
import { Hero, ThumbnailSection } from 'modules/home'
import { ThumbnailBoxProps } from 'modules/home/ThumbnailSection'
import MedicalIcon from '../assets/icons/MedicalIcon'
import MapIcon from '../assets/icons/MapIcon'
import LockIcon from '../assets/icons/LockIcon'
import styled from '@emotion/styled-base'

const Section = Content.withComponent('section')

const CheckButton = styled(UnstyledButton)`
  height: 60px;
  padding: 20px;
  text-transform: uppercase;
  font-weight: 600;
  font-family: IBM Plex Sans;
  background: #3389fe;
  border-radius: 4px;
`

const thumbnailsItem: ThumbnailBoxProps[] = [
  {
    title: 'Daftar RS Rujukan',
    description: 'Daftar rumah sakit di seluruh Indonesia yang menerima rujukan kasus COVID-19.',
    icon: <MedicalIcon fill="#3389fe" height={80} />,
    link: '/'
  },
  {
    title: 'Peta Hasil Periksa',
    description: 'Hasil periksa mandiri para pengguna dikumpulkan dalam satu peta.',
    icon: <MapIcon fill="#3389fe" height={80} />,
    link: '/'
  },
  {
    title: 'Panduan Karantina',
    description: 'Perlu menjalani karantina? Cari tahu panduan lengkapnya di sini.',
    icon: <LockIcon fill="#3389fe" height={80} />,
    link: '/'
  }
]
const IndexPage: NextPage<{}> = () => (
  <PageWrapper>
    <Hero />
    <Section>
      <Column>
        <Stack spacing="xxl">
          <Box display="flex" alignItems="center" justifyContent="center">
            <CheckButton type="button">Mulai Periksa</CheckButton>
          </Box>
          <ThumbnailSection title="Siapkan diri menghadapi COVID-19 dengan informasi berikut: " items={thumbnailsItem} />
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
)

export default IndexPage

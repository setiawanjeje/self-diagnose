import { Connection } from "typeorm";
import { Response } from "express";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalController } from "../../../src/controllers/HospitalController";
import { HospitalService } from "../../../src/services/HospitalService";
import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";

describe("HospitalController", () => {
    let db: Connection;
    let hospitalRepository: HospitalRepository;
    let hospitalService: HospitalService;
    let hospitalController: HospitalController;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        hospitalRepository = db.getCustomRepository(HospitalRepository);
        hospitalService = new HospitalService(hospitalRepository);
        hospitalController = new HospitalController(hospitalService);
        await hospitalRepository.save(HospitalSeed);
    });

    afterAll(() => db.close());

    it("GET /api/hospitals", async () => {
        var res : Response
        const result = await hospitalController.getAll(res);
        expect(result[0].id).toBe(HospitalSeed[0].id.toString());
        expect(result[0].province).toBe(HospitalSeed[0].province);
    });

    it("GET /api/hospitals/:province", async () => {
        var res : Response 
        const result = await hospitalController.get(HospitalSeed[0].province,res);
        expect(result[0].province).toEqual(HospitalSeed[0].province);
    });
});

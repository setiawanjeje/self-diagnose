import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { PostRepository } from "../../../src/repositories/PostRepository";
import { UserRepository } from "../../../src/repositories/UserRepository";
import { PostService } from "../../../src/services/PostService";
import { UserSeed } from "../../utils/seeds/UserTestSeed";
import { Post } from "../../../src/entities/Post";

describe("PostService", () => {
    let db: Connection;
    let userRepository: UserRepository;
    let postRepository: PostRepository;
    let postService: PostService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        userRepository = db.getCustomRepository(UserRepository);
        await userRepository.save(UserSeed);
        postRepository = db.getCustomRepository(PostRepository);
        postService = new PostService(postRepository);
    });

    afterAll(() => db.close());

    const request = {
        title: "Test title.",
        content: "It is test content.",
        previewContent: "It is test content.",
        user: {
            id: "6d2deecf-a0f7-470f-b31f-ede0024efece",
            realName: "Hello jest",
            email: "hellojest@gmail.com",
        },
    };

    const updateRequest = {
        title: "Update title.",
        content: "Update content.",
    };

    let newPostId: string;

    it("Create post", async () => {
        const newPost = await postService.createPost(request as any);
        newPostId = newPost.id;
        expect(newPost.title).toBe(request.title);
        expect(newPost.content).toBe(request.content);
    });

    it("Id finds a matching post and returns post information", async () => {
        const post = await postService.getPostById(newPostId);
        expect(post).toBeInstanceOf(Post);
        expect(post.title).toBe(request.title);
        expect(post.content).toBe(request.content);
    });

    it("Returns the list of posts", async () => {
        const posts = await postService.getPosts(0, 20);
        expect(posts[0].title).toBe(request.title);
        expect(posts[0].previewContent).toBe(request.previewContent);
        expect(posts[0].user.realName).toBe(request.user.realName);
        expect(posts[0].user.email).toBe(request.user.email);
    });
});

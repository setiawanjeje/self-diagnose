/* eslint-disable @typescript-eslint/camelcase */
import app from "../utils/testApp";
import { agent, Response } from "supertest";
import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { UserRepository } from "../../../src/repositories/UserRepository";
import { UserSeed } from "../../utils/seeds/UserTestSeed";
import { generateAccessToken, generateRefreshToken } from "../../../src/middlewares/AuthMiddleware";

let db: Connection;
let userRepository: UserRepository;
let testRefreshToken: string;

const setHeader = (token: string): { Authorization: string; Accept: string } => ({
    Authorization: `Bearer ${token}`,
    Accept: "application/json",
});

const user = {
    id: "6d2deecf-a0f7-470f-b31f-ede0024efece",
    realName: "Hello jest",
    email: "hellojest@gmail.com",
};

beforeAll(async () => {
    db = await createMemoryDatabase();
    userRepository = db.getCustomRepository(UserRepository);
    await userRepository.save(UserSeed);
});

afterAll(async done => {
    await db.close();
    done();
});

describe("GET /api/auth/user", () => {
    it("200: User information returned successfully", done => {
        const token = generateAccessToken(user as any);
        agent(app)
            .get("/api/auth/user")
            .set(setHeader(token))
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.user.email).toEqual("hellojest@gmail.com");
                expect(body.user.realName).toEqual("Hello jest");
                done();
            });
    });

    it("401: Failed to return user information with unauthorized AccessToken", done => {
        agent(app)
            .get("/api/auth/user")
            .set(setHeader("notToken"))
            .expect(401)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                done();
            });
    });
});

describe("POST /api/auth/register", () => {
    it("200: Successful registration", done => {
        agent(app)
            .post("/api/auth/register")
            .set(setHeader(""))
            .send({
                realName: "Hello dev",
                email: "dev.jhyeok@gmail.com",
                password: "test1234",
            })
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.user.email).toEqual("dev.jhyeok@gmail.com");
                expect(body.user.realName).toEqual("Hello dev");
                expect(body.accessToken).toBeTruthy();
                expect(body.refreshToken).toBeTruthy();
                done();
            });
    });

    it("200: Member registration fails with an email already in use", done => {
        agent(app)
            .post("/api/auth/register")
            .set(setHeader(""))
            .send({
                realName: "Hello dev",
                email: "dev.jhyeok@gmail.com",
                password: "test1234",
            })
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.error).toBeTruthy();
                expect(body.message).toEqual("User already exists.");
                done();
            });
    });
});

describe("POST /api/auth/login", () => {
    it("200: Successful login", done => {
        agent(app)
            .post("/api/auth/login")
            .set(setHeader(""))
            .send({
                email: "dev.jhyeok@gmail.com",
                password: "test1234",
            })
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.accessToken).toBeTruthy();
                expect(body.refreshToken).toBeTruthy();
                testRefreshToken = body.refreshToken;
                done();
            });
    });

    it("401: Login fails due to wrong email or password", done => {
        agent(app)
            .post("/api/auth/login")
            .set(setHeader(""))
            .send({
                email: "dev.jhyeok@gmail.com",
                password: "notpassword",
            })
            .expect(401)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                done();
            });
    });
});

describe("POST /api/auth/token/refresh", () => {
    it("200: Successful token reissue", done => {
        agent(app)
            .post("/api/auth/token/refresh")
            .set(setHeader(""))
            .send({
                refresh_token: testRefreshToken,
                grant_type: "refresh_token",
            })
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.accessToken).toBeTruthy();
                expect(body.refreshToken).toBeTruthy();
                done();
            });
    });

    it("401: JWT Re-issue fails with non-format tokens", done => {
        agent(app)
            .post("/api/auth/token/refresh")
            .set(setHeader(""))
            .send({
                refresh_token: "notToken",
                grant_type: "refresh_token",
            })
            .expect(401)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.message).toBe("Invalid or Missing JWT token");
                done();
            });
    });

    it("401: Re-issue fails with tokens that do not match user information", done => {
        const token = generateRefreshToken(user as any);
        agent(app)
            .post("/api/auth/token/refresh")
            .set(setHeader(""))
            .send({
                refresh_token: token,
                grant_type: "refresh_token",
            })
            .expect(401)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body.message).toBe("User information and RefreshToken do not match.");
                done();
            });
    });
});

import { EntityRepository, Repository } from "typeorm";
import { Hospital } from "../entities/Hospital";
import { logger } from "../utils/Logger";
import Tabletop from "tabletop";
import { cache } from "../utils/Cache";

var publicSpreadsheetUrl =
    "https://docs.google.com/spreadsheets/d/1GssYbmC1kVrZj0wNAtPa0yIsKcacztx23FhABn1azic/edit?usp=sharing";

@EntityRepository(Hospital)
export class HospitalRepository extends Repository<Hospital> {
    public getHospitals(): Promise<Array<Hospital>> {

        let hospitals: Array<Hospital> = [];
        let key: string = `getHospital`;

        if(cache.has(key)){
            return new Promise((resolve, reject) => {
                try{
                    logger.info("getHospitals() fetching hospital list from cache");
                    resolve(cache.get(key));
                }catch(err){
                    logger.error("getHospital() error " + err);
                    reject(err)
                }
            });
        }else{
            return new Promise((resolve, reject) => {
                Tabletop.init({
                    key: publicSpreadsheetUrl,
                    callback: function(data, tabletop) {
                        logger.info("getHospitals() fetching hospital list");

                        // Fetch hospitals listed in FixedData only
                        hospitals = tabletop.sheets("FixedData").all();
                        cache.set(key,hospitals);
                        resolve(hospitals);
                    },
                    error: function(err) {
                        logger.error("getHospital() error " + err);
                        reject(err);
                    },
                    orderby: "id",
                });
            });
        }
    }

    public getHospitalsByProvince(name: string): Promise<Array<Hospital>> {

        let hospitals: Array<Hospital> = [];
        let key: string = `getHospitalsByProvince_${name}`;

        if(cache.has(key)){
            return new Promise((resolve, reject) => {
                try{
                    logger.info("getHospitals() fetching hospital list from cache");
                    resolve(cache.get(key));
                }catch(err){
                    logger.error("getHospital() error " + err);
                    reject(err);
                }
            });
        }else{
            return new Promise((resolve, reject) => {
                Tabletop.init({
                    key: publicSpreadsheetUrl,
                    callback: async function(data, tabletop) {
                        var result = tabletop.sheets("FixedData").all();
                        logger.info("getHospitalsByProvince() fetching hospital list");
                        // Filter based on province name
                        await result.forEach(element => {
                            if (element["province"].toLowerCase().includes(name.toLowerCase())) {
                                hospitals.push(element);
                            }
                        });
                        cache.set(key,hospitals);
                        resolve(hospitals);
                    },
                    error: function(err) {
                        logger.error("getHospitalByProvince() error " + err);
                        reject(err);
                    },
                    orderby: "id",
                })
            });
        }
    }
}

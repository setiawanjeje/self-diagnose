import { AuthService } from "../services/AuthService";
import { JsonController, Post, BodyParam, UseBefore, Get, Res, Body, HttpCode } from "routing-controllers";
import {
    checkAccessToken,
    checkRefreshToken,
    generateAccessToken,
    generateRefreshToken,
} from "../middlewares/AuthMiddleware";
import { Response } from "express";
import { UserService } from "../services/UserService";
import { User } from "../entities/User";
import { OpenAPI } from "routing-controllers-openapi";

@JsonController("/auth")
export class AuthController {
    constructor(private authService: AuthService, private userService: UserService) {}

    @HttpCode(200)
    @Post("/login")
    @OpenAPI({
        summary: "Standard login route",
        statusCode: "200",
    })
    public async login(
        @BodyParam("email") email: string,
        @BodyParam("password") password: string,
        @Res() res: Response,
    ) {
        const user = await this.authService.validateUser(email, password);

        if (!user) {
            return res.status(401).send({ message: "Not authorized." });
        }

        const accessToken = generateAccessToken(user);
        const refreshToken = generateRefreshToken(user);
        await this.authService.saveRefreshToken(user, refreshToken);

        return {
            accessToken: accessToken,
            refreshToken: refreshToken,
        };
    }

    @HttpCode(200)
    @Post("/register")
    @OpenAPI({
        summary: "Register user",
        statusCode: "200",
    })
    public async register(@Body() user: User) {
        const isDuplicateUser = await this.userService.isDuplicateUser(user.email);

        if (isDuplicateUser) {
            return {
                error: true,
                message: "User already exists.",
            };
        }

        const newUser = await this.userService.createUser(user);

        const accessToken = generateAccessToken(newUser);
        const refreshToken = generateRefreshToken(newUser);
        await this.authService.saveRefreshToken(newUser, refreshToken);

        const userInfo = {
            id: newUser.id,
            realName: newUser.realName,
            email: newUser.email,
        };

        return {
            user: userInfo,
            accessToken: accessToken,
            refreshToken: refreshToken,
        };
    }

    @HttpCode(200)
    @Post("/token/refresh")
    @OpenAPI({
        summary: "Reissue token",
        description: "Reissuing AccessToken using RefreshToken (Refresh)",
        statusCode: "200",
    })
    @UseBefore(checkRefreshToken)
    public async refreshToken(@Res() res: Response) {
        const userId = res.locals.jwtPayload.userId;
        const refreshToken = res.locals.token;

        const user = await this.authService.validateUserToken(userId, refreshToken);

        if (!user) {
            return res.status(401).send({ message: "User information and RefreshToken do not match." });
        }

        const accessToken = generateAccessToken(user);

        return {
            accessToken: accessToken,
            refreshToken: refreshToken,
        };
    }

    @HttpCode(200)
    @Get("/user")
    @OpenAPI({
        summary: "User information",
        description: "User information is returned by AccessToken (used for authentication)",
        statusCode: "200",
        security: [{ bearerAuth: [] }],
    })
    @UseBefore(checkAccessToken)
    public auth(@Res() res: Response) {
        const { userId, userName, userEmail } = res.locals.jwtPayload;

        const user = {
            id: userId,
            realName: userName,
            email: userEmail,
        };

        return {
            user,
        };
    }
}
